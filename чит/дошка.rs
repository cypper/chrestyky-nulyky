use nalgebra::{SMatrix};
use core::fmt;

use crate::гра::{Вислід};


#[derive(Clone, Copy)]
#[derive(Debug)]
#[derive(PartialEq)]
pub enum Клітинка { П, Х, Н }
impl fmt::Display for Клітинка {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Клітинка::П => write!(f, "П"),
            Клітинка::Х => write!(f, "Х"),
            Клітинка::Н => write!(f, "Н"),
        }
    }
}

pub type Поле = SMatrix<Клітинка, 3, 3>;

pub fn створитиПустеПоле() -> Поле {
    return Поле::new(Клітинка::П, Клітинка::П, Клітинка::П,
        Клітинка::П, Клітинка::П, Клітинка::П,
        Клітинка::П, Клітинка::П, Клітинка::П);
}

#[derive(Clone, Copy)]
pub struct Дошка {
    pub поле: Поле,
}
impl fmt::Display for Дошка {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.пустіКлітинки().len())
    }
}

impl Дошка {
    pub fn чиПусте(&self, а: i8, б: i8) -> bool {
        return self.поле[(а as usize, б as usize)] == Клітинка::П;
    }

    pub fn пустіКлітинки(&self) -> Vec<[i8; 2]> {
        let mut вкази = vec![];
        for а in 0..3 {
            for б in 0..3 {
                if self.чиПусте(а, б) {
                    вкази.push([а, б]);
                }
            }
        }
        
        return вкази;
    }

    pub fn вставити(&mut self, а: i8, б: i8, клітинка: Клітинка) -> () {
        self.поле[(а as usize, б as usize)] = клітинка;
    }

    pub fn стан(&self) -> Option<Вислід> {
        if 
            self.поле.column_iter().find(|ряд| ряд.iter().all(|&кл| кл == Клітинка::Х)).is_some() ||
            self.поле.row_iter().find(|ряд| ряд.iter().all(|&кл| кл == Клітинка::Х)).is_some() ||
            [
                [self.поле[(0, 0)], self.поле[(1, 1)], self.поле[(2, 2)]],
                [self.поле[(0, 2)], self.поле[(1, 1)], self.поле[(2, 0)]]
                // [self.поле[(0, 0)], self.поле[(1, 1)], self.поле[(2, 2)], self.поле[(3, 3)]],
                // [self.поле[(0, 3)], self.поле[(1, 2)], self.поле[(2, 1)], self.поле[(3, 3)]]
            ].iter().find(|ряд| ряд.iter().all(|&кл| кл == Клітинка::Х)).is_some() {
            return Some(Вислід::ПЕРЕМОГА_А)
        }

        if 
            self.поле.column_iter().find(|ряд| ряд.iter().all(|&кл| кл == Клітинка::Н)).is_some() ||
            self.поле.row_iter().find(|ряд| ряд.iter().all(|&кл| кл == Клітинка::Н)).is_some() ||
            [
                [self.поле[(0, 0)], self.поле[(1, 1)], self.поле[(2, 2)]],
                [self.поле[(0, 2)], self.поле[(1, 1)], self.поле[(2, 0)]]
                // [self.поле[(0, 0)], self.поле[(1, 1)], self.поле[(2, 2)], self.поле[(3, 3)]],
                // [self.поле[(0, 3)], self.поле[(1, 2)], self.поле[(2, 1)], self.поле[(3, 3)]]
            ].iter().find(|ряд| ряд.iter().all(|&кл| кл == Клітинка::Н)).is_some() {
            return Some(Вислід::ПЕРЕМОГА_Б)
        }

        let вислід = self.поле.iter().find(|&&кл| кл == Клітинка::П);
        if вислід.is_none() { return Some(Вислід::НІЧІЯ) };

        return None;
    }
}
