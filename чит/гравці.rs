use core::fmt;
use rand::seq::SliceRandom;
use trees::Tree;
use std::io::{stdin};

use crate::дошка::{Дошка};
use crate::гра::{Хід};
use crate::дерево_ходів_гри::{ВузолДерева, вДеревоХодів};


#[derive(Clone, Copy)]
#[derive(PartialEq)]
pub enum ЗнаменоГравця { А, Б }
impl fmt::Display for ЗнаменоГравця {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ЗнаменоГравця::А => write!(f, "А"),
            ЗнаменоГравця::Б => write!(f, "Б"),
        }
    }
}

pub struct ГравецьАбиДе {
    pub знаменоГравця: ЗнаменоГравця,
}

pub struct ГравецьЛюдина {
    pub знаменоГравця: ЗнаменоГравця,
}

pub struct ГравецьНадрозум {
    pub знаменоГравця: ЗнаменоГравця,
}

pub enum Гравець {
    ГравецьАбиДе(ГравецьАбиДе),
    ГравецьЛюдина(ГравецьЛюдина),
    ГравецьНадрозум(ГравецьНадрозум),
}

pub trait ХідГравця {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<Хід>) -> Option<Хід>;
}

impl ХідГравця for ГравецьАбиДе {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<Хід>) -> Option<Хід> {
        let вкази = дошка.пустіКлітинки();
        let можлВказ = вкази.choose(&mut rand::thread_rng());
        if можлВказ.is_some() {
            let вказ = можлВказ.unwrap();
            return Some(Хід { а: вказ[0], б: вказ[1] })
        } else {
            return None
        }
    }
}

impl ХідГравця for ГравецьЛюдина {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<Хід>) -> Option<Хід> {
        let mut вхід = String::new();
        stdin().read_line(&mut вхід).expect("error: unable to read user input");
        let mut можлВказ = вхід.split(",").map(|ст| ст.trim()).map(|ст| ст.parse::<i8>().expect("please give me correct string number!"));
        let можлА = можлВказ.nth(0);
        let можлБ = можлВказ.nth(0);

        if можлА.is_some() && можлБ.is_some() {
            let а = можлА.unwrap();
            let б = можлБ.unwrap();
            return Some(Хід { а, б })
        } else {
            return None
        }
    }
}

impl ХідГравця for ГравецьНадрозум {
    fn вХід(&self, дошка: &Дошка, минХоди: &Vec<Хід>) -> Option<Хід> {
        let деревоХодів: Tree<ВузолДерева> = вДеревоХодів();

        let mut поточнеДерево = деревоХодів.root();
        for хід in минХоди {
            let підД = поточнеДерево.iter().find(|підД| {
                let хідД = підД.data().хід.unwrap();
                return хід.а == хідД.а && хід.б == хідД.б;
            });

            if підД.is_none() { return None };
            поточнеДерево = підД.unwrap();
        }
        

        поточнеДерево.iter().for_each(|підд| print!("{},", підд.data().оцінка));
        let найкХід = if self.знаменоГравця == ЗнаменоГравця::А {
            поточнеДерево.iter().max_by_key(|підд| підд.data().оцінка)
        } else {
            поточнеДерево.iter().min_by_key(|підд| підд.data().оцінка)
        };
        if найкХід.is_none() { return None };

        return найкХід.unwrap().data().хід;
    }
}